Cyblion Dashboard
==========

**วิธีติดตั้ง**

```

git clone https://gitlab.com/cipherflow/freeboard
cd freeboard

```
แนะนำให้ติดตั้ง serve ซึ่งเป็น webserver ขนาดเล็กที่ใช้รัน web แบบ static สำหรับใช้ตอน dev
```

npm install serve -g

```
และรันคำสั่งนี้ จะเปิด webserver ที่ port 50000
```

serve -p 50000

```
เข้าใช้งานเว็บ freenoard ได้ที่ http://localhost:50000

**การใช้งานแบบที่ 1 : Dev Mode**

วินี้นี้เหมาะสำหรับขั้นตอน dev เพราะสามารถรันทดสอบได้เลย ไม่ต้องเสียเวลา build แต่การเข้าเว็บจะต้องใช้ไฟล์ index-dev.html

**การใช้งานแบบที่ 2 : Build Mode**

การนำไปใช้งานจริงแนะนำให้ใช้โหมดนี้ ซึ่งจะมีขั้นตอนเพิ่มขึ้นคือ ต้องรันคำสั่งนี้เพื่อทำการ build และ minify css และ js เข้าด้วยกันก่อน
```
grunt

```
จากนั้นจะสามารถรัน webserver และเข้าหน้าเวบได้ ซึ่งโหมดนี้จะใช้ไฟล์ index.html เป็น main


ไฟล์ที่จำเป็นต้องใช้ตอน production จะอยู่ใน directory นี้
index.html
css/
img/
js/


**สิ่งที่ควรทราบ**

- source ของ cyblion mqtt datasource อยู่ที่ plugins/cyblion
- style จะอยู่ที่ไฟล์ lib/css/freeboard/style.css ปรับสีได้เลย แต่ถ้าจะเอาไปใช้ใน index.html ต้องใช้คำสั่ง grunt เพื่อ build ก่อนครั้งนึง 
- Freeboard เวอร์ชั่นนี้ มีการเพิ่มฟังก์ชั่นรับ device credential จาก URL hash ในรูปแบบข้างล่างนี้ กลไกการทำงานคือ มันจะเข้าไปสร้าง datasource ชื่อ dashboard ขึ้นมาให้โดยอัตโนมัติ ซึ่งหากเรามี datasource ชื่อ dashboard เดิมอยู่มันจะเขียนทับ datasource นั้นไปเลย

```
http://localhost:50000#DEVICEID:DEVICETOKEN
```

- Freeboard เวอร์ชั่นนี้ถูกดัดแปลงเป็นพิเศษให้เหมาะกับ developer ตอนใช้งานจริงไม่จำเป็นต้องใช้ freeboard ตัวนี้ก็ได้ ฟีเจอร์ที่เพิ่มมาคือดัดแปลงปุ่ม SAVE FREEBOARD จากที่เคยเป็นการ export อออกมาเป็นไฟล์ json เปลี่ยนให้เป็นการเขียนลงไปใน LocalStorage แทน และนอกจากนี้ เวลาโหลดเว็บ freeboard ครั้งแรกใน browser มันจะโหลด freeboard configuration จาก LocalStorage กลับมาอัตโนมัติ ซึ่งเวอร์ชั่นที่ใช้งานจริง flow จะไม่ได้เป็นแบบนี้ ดังนั้นไม่แนะนำให้เอา freeboard ตัวนี้ไปใช้ นอกจากจะนำไปแก้ไขดัดแปลงต่อเอง

- ห้ามเปิด freeboard ที่ใช้ deviceid เดียวกันมากกว่า 1 หน้าอย่างเด็ดขาด เพราะ connection จะติดดับสลับกันไปมา และอาจถูก NETPIE แบน device ได้



**เดโม**

1. รัน freeboard

2. เปิด web ด้วย url ลักษณะข้างล่างนี้ โดยใช้ DEVICEID และ DEVICETOKEN จาก NETPIE
```
http://localhost:50000#DEVICEID:DEVICETOKEN
```

3. import ไฟล์ config จาก examples-jsonconfig/jsonconfig-cyblion.json

4. ตัวอย่างนี้แสดงการส่งผ่านข้อมูลผ่าน MQTT ทั้งทาง netpie ปกติ (@msg) และทาง fhe (@cstore) แต่ช่องทาง fhe ยังไม่ได้ implement การเข้าและการถอดรหัส ยังต้องเข้าไปเขียนฟังก์ชั่น cyblion_decrypt() ในไฟล์นี้ ให้ทำการส่ง ciphertext เข้าไปที่ API ของชั้น native app ให้ถอดรหัสและ plain text กลับมา return

https://gitlab.com/cipherflow/freeboard/-/blob/main/plugins/cyblion/cyblion.decryptor.js#L2-4 
